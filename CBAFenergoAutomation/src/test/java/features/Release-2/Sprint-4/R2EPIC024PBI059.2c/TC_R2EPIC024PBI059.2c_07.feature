#Test Case: TC_R2EPIC024PBI059.2c_07
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2c_07

Scenario: Validate refresh results for a different LE Category will not impact on the doc requirements on existing COB cases whether open or closed


#Pre-Requisite : An in-flight COB case
#Positive test
	 Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
    #Positive Test
    #Validate no new documents are added as per new LE category
     Validate no new document(s) are added as per new LE Category

#NextTest     
#Pre-Requisite : An completed COB case
  Given I login to Fenergo Application with "RM:IBG-DNE" 
     When I complete "NewRequest" screen with key "Corporate" 
      And I complete "CaptureNewRequest" with Key "C1" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button 
     When I complete "ReviewRequest" task 
     Then I store the "CaseId" from LE360 
  
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
     When I search for the "CaseId"
     Then I store the "CaseId" from LE360  
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
  
     When I navigate to "EnrichKYCProfileGrid" task 
     When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
     When I complete "AddAddressFAB" task 
     Then I store the "CaseId" from LE360 
     When I complete "EnrichKYC" screen with key "C1" 
      And I click on "SaveandCompleteforEnrichKYC" button 
  
     When I navigate to "CaptureHierarchyDetailsGrid" task 
     When I add AssociatedParty by right clicking 
     When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
     When I complete "AssociationDetails" screen with key "Director" 
     When I complete "CaptureHierarchyDetails" task 
  
     When I navigate to "KYCDocumentRequirementsGrid" task 
    #Positive Test
    #Validate no new documents are added as per new LE category
     Validate no new document(s) are added as per new LE Category
  