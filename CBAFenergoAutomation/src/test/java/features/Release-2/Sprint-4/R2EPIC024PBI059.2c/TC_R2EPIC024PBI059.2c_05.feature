#Test Case: TC_R2EPIC024PBI059.2b_05
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2b_05

Scenario: Validate the documents in the document matrix will add/change based on new LE Category in an RR case.

#Pre-Requisite - An already completed COB case.
 
     Given I login to Fenergo Application with "SuperUser" 
	 When I search for the "CaseId" 
	 And I initiate "Regular Review" from action button 
     When I navigate to "CloseAssociatedCaseGrid" task 
     When I complete "CloseAssociatedCase" task 
     When I navigate to "ValidateKYCandRegulatoryGrid" task 
     Then I store the "CaseId" from LE360 
     When I complete "ValidateKYC" screen with key "C1" 
      And I click on "SaveandCompleteforValidateKYC" button 
     When I navigate to "ReviewRequestGrid" task 
     When I complete "RRReviewRequest" task 
  
     When I navigate to "Review/EditClientDataTask" task 
     When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
     When I complete "EnrichKYC" screen with key "RegularReviewClientaDataSanity" 
     And I click on "SaveandCompleteforEnrichKYC" button 
     
     When I navigate to "KYCDocumentRequirementsGrid" task 
   #Positive Test
   #Validate new documents are added as per new LE category
     Validate new in-scope document(s) are added as per new LE Category
   #Negative Test
	 Validate document(s) based on old Category are removed
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task