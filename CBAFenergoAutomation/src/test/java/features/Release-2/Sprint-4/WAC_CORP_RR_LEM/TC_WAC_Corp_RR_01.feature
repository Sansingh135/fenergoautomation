#Test Case: TC_WAC_Corp_RR_01
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corp_RR_01

  Scenario: RR-Derive the overrall risk rating as Very High (COB overall risk rating is Medium-Low)
    #Refer to TC21 in the WAC Corp Data Sheet