#Test Case: TC_R2EPICCRPBI001_03
  #PBI: R2EPICCRPBI001
  #User Story ID: Data_ADD_001
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @RR
  Scenario: PCG - Validate the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" under "Address Information section" in "Review Client Data" Stage
   #Validate the data entered during COB flow should be visible in RR workflow
   
   #Validate the maximum length for the fields "Address line 1", "Address line 2", and "Town/ city" should be 35 characters
   #Validate only "  " " ' ( ) + , - . / : ? "special characters are allowed for the fields "Address line 1", "Address line 2", and "Town/ city"
   #Validate the validation messages when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #validate "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #Validate When referring back to Review Client Data stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
	#Validate when the user refer back and edit the above mentioned fields 	with Length of the character is more than 35 & other than specified special characters, a validation message should be displayed.
   #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
   #KYC Conditions section - No change (OOTB feature)
    #################################################################################################
    #PreCondition: Create entity with client type as PCG and confidential as PCG.
    #######################################################################################
   Given I login to Fenergo Application with "RM:PCG"
    When I complete "NewRequest" screen with key "PCG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:PCG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:PCG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: PCG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    #Initiate RR
    And I initiate "Regular Review" from action button
 
    When I navigate to "ClosedAssociatedCases" task
    When I complete the "ClosedAssociatedCases" task
    
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
     When I navigate to "Review Request Details" task
    When I complete "Review Request Details" task
    When I navigate to "Review/Edit Client Data" task
    #Review/Edit Client Data screen
    And I select "Edit" option from the "Action" button on "Addresses" subflow
    #Verify the data entered during COB flow should be visible in RR workflow   
    And I Validate all the updated values are retained in "Address" screen under section " Address Information"
    #Verify the validation messages when user enters more than specified character limit and other than specified special characters for above mentioned fields
    #verify "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 1"
    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I write "Address-12345678("Resident")####@@@@@Adresss234 for field "Address line 2"
    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I write "Australia-12345678("Resident")####@@@@@Adresss234 for field "Town/ City"
    And I validate the validation message "You've reached the maximum length. Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I validate "save " buton is not enable 
    
    #Verify the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" under "Address Information "section
    When I write "Address-12345678("Resident123")" for field "Address line 1"
    When I write "Address-12345678("Resident45")" for field "Address line 2"
    When I write "Address-12345678("Resident34")" for field "Town/ city"
    And I validate the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" 
    And I click on "Save" button   
    When I complete "Review/Edit Client Data" task
    When I naviagate to "KYC Document Requirements" task
    When I Complete "KYC Document Requirements" task
    
    #Verify When referring back to Review Client Data stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
    When I select "Refer" from action button
    When I select "Review Client Data" for field "RefertoStage"
    when I write "Test" for field "Referral Reason"
    And I click on "Refer" button
    Then I see "Closed AssociatedCases" task is generated
    When I navigate to "Closed AssociatedCases" task
    When I complete "Closed AssociatedCases" task
    When I navigate to "ValidateKYCandRegulatoryData" task
    When I complete the "ValidateKYCandRegulatoryData" task
     When I navigate to "Review Request Details" task
    When I complete "Review Request Details" task
    When I navigate to "Review/Edit Client Data" task
    #Review/Edit Client Data screen
    And I select "Edit" option from the "Action" button on "Addresses" subflow
    And I Validate all the updated values are retained in "Address" screen
    #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
    And I verify "Save and Add another" & "Save" button is not enabled
    #Validate when the user refer back and edit the above mentioned fields 	with Length of the character is more than 35 & other than specified special characters, a validation message should be displayed.
    And I write "Address-12345678("Correspondance")####@@@@@Adresss234 for field "Address line 1"
    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I write "Address-12345678("Correspondance")####@@@@@Adresss234 for field "Address line 2"
    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I write "Australia-12345678("Correspondance")####@@@@@Adresss234 for field "Town/ City"
    And I validate the validation message "You've reached the maximum length.Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen
    And I validate "save " buton is not enable 
    #Verify the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" under "Address Information "section
    When I write "Address-12345678("Australia")" for field "Address line 1"
    When I write "Address-12345678("Rusia")" for field "Address line 2"
    When I write "Address-12345678("India")" for field "Town/ city"
    And I validate the maximum length and special characters for the fields "Address line 1", "Address line 2", and "Town/ city" 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    #Initiate LEM
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    #Verify Field "Purpose of Account/Relationship"should be come between "Channel & Interface" and "Residential Status" fields
    And I Validate the field "Purpose of Account/Relationship" is present between field Channel & Interface and Residential Status
    #Verify the field "Details for Purpose of Account/Relationship" is not visible
    And I validate field "Details for Purpose of Account/Relationship" is not visible
    #Verify the field validation of 'Details Purpose of Account/Relationship' when Purpose of Account/Relationship type lov is selected as"Other"
    And I select "Other" for "Purpose of Account/Relationship" field
    And I validate "Details Purpose of Account/Relationship" field is visible
   
    #Verify the default value is set to No for the field "CIF Creation required"
    And I Validate the default value is set to "No" for the field "CIF Creation required"
     ##Validate if CIF Creation required field should be greyed out
    And I assert "CIF Creation required" fields is greyed out
    
    #verify field behavior for the fields Purpose of Account/Relationship and Details Purpose of Account/Relationship
    And I validate the following fields in "Customer Details" section
      | Label                                       | Field Type                | Visible | Editable | Mandatory | Field Defaults To |
      | Purpose of Account/Relationship             | Multiselect drop-down     | Yes     | Yes      | No        |                   |
      | Details Purpose of Account/Relationship     | Alphanumeric              | Yes     | Yes      | No        |                   |
      | CIF Creation required                       | Check box                 | Yes     |  No      | No        | No                |
    #Validate the Purpose of Account/Relationship LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Purpose of Account/Relationship" field
    #Refer PBI for LOV list  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    