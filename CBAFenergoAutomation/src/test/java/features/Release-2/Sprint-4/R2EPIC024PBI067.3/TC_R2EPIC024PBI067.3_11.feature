#Test Case: TC_R2EPIC024PBI067.3_11
  #PBI: R2EPIC024PBI067.3
  #User Story ID: BL_067_004
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @LEM
  Scenario: NBFI - Validate the field behaviours in "Update Customer Details" Screens in Maintenance Request stage:
  #Field behavior in Business Details  section (1 new Data field should be added)
  #Validate fields "Value of Initial Deposit (AED),Source of Initial Deposit (AED),Details for Source of Initial Deposit,Country of Source of Initial Deposit, Projected Annual Business Turnover (AED) and Annual Business Turnover of Group (AED)" should not be visible on the UI
  #Validate field "Annual Business Turnover (AED) " should visible on screen under "Enrich KYC Profile" 
  #Validate the LoVs are displayed in the order mentioned in the PBI-LOV tab for the fields "Annual Business Turnover (AED)
  #Field behavior in Source Of Funds And Wealth Details  section (2 new Data field should be added, 2 new field should be hidden)
  #Validate fields "Details for Legal Entity Source of Income & Wealth,Details for Legal Entity Source of Funds" should be hidden from the UI
  #Validate fields "Legal Entity Source of Income & Wealth and Legal Entity Source of Funds" should visible on screen under "Source Of Funds And Wealth Details" section
  #Validate the LoVs are displayed in the order mentioned in the PBI-LOV tab for the fields "Legal Entity Source of Income & Wealth and Legal Entity Source of Funds" 
  #################################################################################
  #PreCondition: Create entity with client type as NBFI and confidential as NBFI with product type "Call Account or Savings ".
  ####################################################################################
  Given I login to Fenergo Application with "RM:NBFI"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:NBFI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate LEM
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedChanges" task
    When I navigate to "Update Customer Details" task
    # Update Customer Details screen
    #And I validate the following fields are not visible in "Business Details " section
   And I check that below fields are  not visible
      | FieldLabel                               |
      | Value of Initial Deposit (AED)           |
      | Source of Initial Deposit (AED)          |
      | Details for Source of Initial Deposit    |
      | Country of Source of Initial Deposit     |
      | Projected Annual Business Turnover (AED) |
      | Annual Business Turnover of Group (AED)  |
   
   #And I validate the following fields are visible in "Business Details " section
   And I check that below fields are visible
      | FieldLabel                               |
      | Annual Business Turnover (AED)           |
   
  #verify field behavior for the fields "Annual Business Turnover (AED)"
   And I validate the following fields in "KYC Conditions" section
    | Label                                 | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | Annual Business Turnover (AED)        | drop-down     | Yes     | Yes      | No        |                   |
    #Validate the Value of Annual Business Turnover (AED) LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Annual Business Turnover (AED)" field
    #Refer PBI for LOV list 
    
    #And I validate the following fields are not visible in "Source Of Funds And Wealth Details" section
   And I check that below fields are  not visible
      | FieldLabel                                          |
      | Details for Legal Entity Source of Income & Wealth  |
      | Details for Legal Entity Source of Funds            |
    #Condition: field "Legal Entity Source of Income & Wealth" should be visible if product is Product is "Call Account or Savings Account"   
   #And I validate the following fields are visible in "Source Of Funds And Wealth Details" section
   And I check that below fields are visible
      | FieldLabel                               |
      | Legal Entity Source of Income & Wealth   |
      | Legal Entity Source of Funds             |
      
    #verify field behavior for the fields "Legal Entity Source of Income & Wealth"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                                    | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
    | Legal Entity Source of Income & Wealth   | Multi-Select drop-down| Yes     | Yes      | No        |  Select...        |   
    #Validate the Value of Initial Deposit (AED) LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Value of Initial Deposit (AED)" field
    #Refer PBI for LOV list
    
    #Verify the field validation of 'Details for Legal Entity Source of Income & Wealth' when select value as "Other " from "Legal Entity Source of Income & Wealth" type field 
    And I select "Other" for "Legal Entity Source of Income & Wealth" field
    And I validate 'Details for Legal Entity Source of Income & Wealth' field is visible
    #verify field behavior for the conditional field "Details for Legal Entity Source of Income & Wealth"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                                              | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | Details for Legal Entity Source of Income & Wealth | Alphanumeric  | Yes     | Yes      | Yes        |                   |
    
    #Verify field "Details for Legal Entity Source of Income & Wealth" should not be visible if user select "Others" as one of the value with additional one or more LOVs for field "Legal Entity Source of Income & Wealth"
    And I select "Other" with additional one more LOV for "Legal Entity Source of Income & Wealth" field
    And I validate 'Details for Legal Entity Source of Income & Wealth' field is not visible
    
   #verify field behavior for the fields "Legal Entity Source of Fund"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                         | Field Type            | Visible | Editable | Mandatory | Field Defaults To |
    | Legal Entity Source of Fund   | Multi-Select drop-down| Yes     | Yes      | No        |  Select...        |   
    #Validate the Value of Legal Entity Source of Fund LoVs are displayed in the order mentioned in the PBI-LOV tab
    And I validate LOVs of "Legal Entity Source of Fund" field
    #Refer PBI for LOV list
    
    #Verify the field validation of 'Details for Legal Entity Source of funds' when select value as "Other " from "Legal Entity Source of Funds" type field 
    And I select "Other" for "Legal Entity Source of funds" field
    And I validate 'Details for Legal Entity Source of funds' field is visible
    #verify field behavior for the conditional field "Details for Legal Entity Source of funds"
   And I validate the following fields in "Source Of Funds And Wealth Details" section
    | Label                                    | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | Details for Legal Entity Source of funds | Alphanumeric  | Yes     | Yes      | Yes        |                   | 
  #Verify field "Details for Legal Entity Source of funds" should not be visible if user select "Others" as one of the value with additional one or more LOVs for "Legal Entity Source of funds" field 
    And I select "Other" with additional one more LOV for "Legal Entity Source of funds" field
    And I validate 'Details for Legal Entity Source of funds' field is not visible 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    