#Test Case: TC_Backlog 057_07
#PBI: Backlog 057
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 057_07

Scenario: Validate when user Manually assign a specific task to a user and refer back and then navigate to 
#the original task.Verify assigned user is updated correctly in the task grid on case details screen
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "SuperUser"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I click in 'Edittask' button from the options button
    When I select "user name" as "KYCMaker: Corporate"
    When I save the details
    #refer the case to "EnrichKYCProfileGrid" task
    When I click on Actions button and click on refer option
    When I select refer to stage as "EnrichKYCProfileGrid" screen and submit the task
    Then I verify the case is referred back to "EnrichKYCProfileGrid" task
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    #Validate "CompleteAMLGrid" task is assigned to the same user who has referred the case(KYCMaker: Corporate)
    When I navigate to "CompleteAMLGrid" task
    Then I see "KYCMaker: Corporate" is displaying as "Assigned user" for "CompleteAMLGrid" task 
    When I navigate to "CompleteAMLGrid" task
    #validate task is displaying under 'MyTasks' grid
    When I navigate to "Mydashboard" task and click on "MyTasks" grid
    Then I validate task is displaying under 'MyTasks' grid
  	#validate task is displaying under 'TeamTasks' grid
    When I navigate to "Mydashboard" task and click on "TeamTasks" grid
    Then I validate task is displaying under 'TeamTasks' grid
    And I complete "CompleteAMLGrid" task
    
    
    
    
