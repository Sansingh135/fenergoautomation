#Test Case: TC_R2EPIC014PBI002_25
#PBI: R2EPIC014PBI001
#User Story ID: Corp/PCG-3,FIG-3
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI002_25

  @To_be_automated
  Scenario: Validate "Compliance Review Date" is visible on LE360-LE details screen for Client type 'Corporate'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate "Compliance Review Date" is visible on LE360-LE Verified details screen
    When I navigate to ‘LEVerifieddetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE Verified details screen

  Scenario: Validate "Compliance Review Date" is visible on LE360-LE details screen for Client type 'FI'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "FI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate "Compliance Review Date" is visible on LE360-LE Verified details screen
    When I navigate to ‘LEVerifieddetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE Verified details screen

  Scenario: Validate "Compliance Review Date" is visible on LE360-LE details screen for Client type 'NBFI'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate "Compliance Review Date" is visible on LE360-LE Verified details screen
    When I navigate to ‘LEVerifieddetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE Verified details screen

  Scenario: Validate "Compliance Review Date" is visible on LE360-LE details screen for Client type 'BBG'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate Risk category as 'medium-Low'
    Then I Select Risk category as 'medium-Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate "Compliance Review Date" is visible on LE360-LE Verified details screen
    When I navigate to ‘LEVerifieddetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE Verified details screen

  Scenario: Validate "Compliance Review Date" is visible on LE360-LE details screen for Client type 'PCG-Entity'
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "PCG-Entity"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    #Validate "Compliance Review Date" is visible on LE360-LE details screen
    When I navigate to ‘LEdetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE details screen
    #Validate "Compliance Review Date" is visible on LE360-LE Verified details screen
    When I navigate to ‘LEVerifieddetails’ task screen
    Then I see "Compliance Review Date" is visible on LE360-LE Verified details screen
