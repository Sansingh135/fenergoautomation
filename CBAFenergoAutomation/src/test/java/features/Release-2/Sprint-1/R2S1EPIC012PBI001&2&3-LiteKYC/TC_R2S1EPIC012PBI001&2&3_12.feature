#Test Case: TC_R2S1EPIC012PBI001&2&3_12
#PBI: R2S1EPIC012PBI001, R2S1EPIC012PBI002, R2S1EPIC012PBI003
#User Story ID: Light_KYC_001, Light_KYC_006, Light_KYC_015
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Lite KYC

  @To_be_automated
  Scenario: Validate if Lite KYC is not triggered when "Does the CDD profile qualify for Lite KYC?" field is selected as "No" for "NBFI" client type and LE Category "Investment Fund "
    ##Validate if risk is not defaulted to "Medium" and editable
    Given I login to Fenergo Application with "RM:NBFI"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - NBFI
    When I complete "Enter Entity details" screen task with ClientEntityType as "NBFI"
    When I complete "Search For Duplicates" screen task
    Then I select "Investment Fund " for Legal Entity Category field
    And I select "Client/Counterparty" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    And I assert "CREATE ENTITY" button is not enabled
    And I select "No" for "Does the CDD profile qualify for Lite KYC?" field
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered
    #Validate if the user is directly taken to Capture Request Details screen of COB workflow
    And I assert user is navigated to "Capture Request Details" screen #COB workflow
    #Test data - Confidential value - NBFI
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    #Add an associated party
    When I navigate to "CaptureHierarchyDetailsGrid" task
    Then I add an associated party
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    ##Validate Doc Matrix requirement for COB work flow with Client Type "NBFI"  and LE Category "Investment Fund " is trigerred
    Then I click on "Save & Complete
    Then I assert Error is thrown to add mandatory documents
    When I add a "DocumentUpload" in KYCDocument # add only mandaotry documents
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    ##Validate if risk is not defaulted to "Medium" and is editable
    And I assert "Risk Category" is blank
    And I assert "Risk Category" is editable
    And I assert "Continue" button is disabled
    And I select "Very High" for "Risk Category" field
    When I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #Relationship Manager Review and Sign-Off
    Then I login to Fenergo Application with "KYCManager"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    Then I login to Fenergo Application with "AVP"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #CIB R&C KYC Approver - AVP Review and Sign-Off
    Then I login to Fenergo Application with "VP"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #CIB R&C KYC Approver - VP Review and Sign-Off
    Then I login to Fenergo Application with "SVP"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #CIB R&C KYC Approver - SVP Review and Sign-Off
    Then I login to Fenergo Application with "CDD"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #Group Compliance (CDD) Review and Sign-Off
    Then I login to Fenergo Application with "BUH:NBFI"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #Business Unit Head Review and Sign-Off
    Then I login to Fenergo Application with "BH:FIG"
    When I search for the "CaseId"
    When I complete "ReviewSignOff" task #Business Head Review and Sign-Off
    