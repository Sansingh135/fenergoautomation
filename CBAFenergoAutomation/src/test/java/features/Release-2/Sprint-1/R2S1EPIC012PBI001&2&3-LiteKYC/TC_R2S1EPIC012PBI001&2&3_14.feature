#Test Case: TC_R2S1EPIC012PBI001&2&3_14
#PBI: R2S1EPIC012PBI001, R2S1EPIC012PBI002, R2S1EPIC012PBI003
#User Story ID: Light_KYC_001, Light_KYC_006, Light_KYC_015
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Lite KYC

  Scenario: Validate if "Does the CDD profile qualify for Lite KYC?" field is not present for Client Type "Corporate"  
   	##Validate if Lite KYC is not triggered when Client Type "Corporate"
    Given I login to Fenergo Application with "RM:IBGDNE"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - Corporate
    When I complete "Enter Entity details" screen task with ClientEntityType as "Corporate"
    When I complete "Search For Duplicates" screen task
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    Then I select "General Partnership" for Legal Entity Category field
    And I select "Client/Counterparty" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered
    #Validate if the user is directly taken to Capture Request Details screen of COB workflow
    And I assert user is navigated to "Capture Request Details" screen #COB workflow
    #Test data - Confidential value - IBGDNE
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task

   
   Scenario: Validate if "Does the CDD profile qualify for Lite KYC?" field is not present for Client Type "PCG-Entity"  
   	##Validate if Lite KYC is not triggered when Client Type "PCG-Entity"
    Given I login to Fenergo Application with "RM:PCG"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - PCG-Entity
    When I complete "Enter Entity details" screen task with ClientEntityType as "PCG-Entity"
    When I complete "Search For Duplicates" screen task
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    Then I select "General Partnership" for Legal Entity Category field
    And I select "Client/Counterparty" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered
    #Validate if the user is directly taken to Capture Request Details screen of COB workflow
    And I assert user is navigated to "Capture Request Details" screen #COB workflow
    #Test data - Confidential value - PCG
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    
    
   Scenario: Validate if "Does the CDD profile qualify for Lite KYC?" field is not present for Client Type "BBG"  
   	##Validate if Lite KYC is not triggered when Client Type "BBG"
    Given I login to Fenergo Application with "RM:BBG"
    When I click on "+" sign to create new request
    When I navigate to "Enter Entity details" screen
    #Test data: Client Type - BBG
    When I complete "Enter Entity details" screen task with ClientEntityType as "PCG-Entity"
    When I complete "Search For Duplicates" screen task
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    Then I select "General Partnership" for Legal Entity Category field
    And I select "Client/Counterparty" for "Legal Entity Role" field
    And I select any value for "Entity of Onboarding" field
    Then I assert "Does the CDD profile qualify for Lite KYC?" field is not visible
    And I assert "CREATE ENTITY" button is enabled
    And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    And I assert "Lite KYC Onboarding" workflow is not triggered
    #Validate if the user is directly taken to Capture Request Details screen of COB workflow
    And I assert user is navigated to "Capture Request Details" screen #COB workflow
    #Test data - Confidential value - BBG
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task