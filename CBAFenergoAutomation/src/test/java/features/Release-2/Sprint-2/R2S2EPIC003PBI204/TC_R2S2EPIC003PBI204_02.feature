#Test Case: TC_R2S2EPIC003PBI204_02
#PBI: R2S2EPIC003PBI204
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2S2EPIC003PBI204_02

  
  Scenario: RR-Validate the LOVs for the fields 'Primary Industry of Operation', 'Secondary Industry of operation' and 'Primary Industry of Operation-UAE' in RR flow
    #Additional Scenario: Existing lov '00005-Individual and Trusts' is renamed as '00005-Trusts
    #Existing Lovs:547, New Lovs:79. Total Lovs: 626
    #Additional Scenario: Refer back and check the values are retained for the three fields
    #Additional Scenario: Verify the values are updated correctly in LE360-LEdetails screen
    #Additional Scenario: Verify the values are updated correctly in Verified LE details of LE360 screen
   