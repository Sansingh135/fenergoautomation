#Test Case: TC_WAC_Corporate_09
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Sasmita
#Last Edited by: Sasmita
Feature: TC_WAC_Corporate_09

  Scenario: COB-Derive Overall Risk Rating as High and all individual attributes risk rating as High
    #Refer to TC09 in the WAC Corp Data Sheet