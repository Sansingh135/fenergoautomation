#Test Case: TC_WAC_Corporate_18
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_18

  Scenario: COB-Refer back and derive the overrall risk rating as High (previous risk rating Medium)
    #Refer to TC18 in the WAC Corp Data Sheet