#Test Case: TC_R2EPIC013PBI002_05
#PBI: R2EPIC013PBI002
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: GLCMS

  Scenario: Verify for unsuccessful request attempts(10 tries) through GLCMS, all the records are getting displayed inside the grid on "Waiting for UID from GLCMS" task for KYC maker.
 
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    #Then I login to Fenergo Application with "Onboarding Maker"
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    #Validate Risk category as 'Low'
    Then I Select Risk category as 'Low'
    And I complete "RiskAssessmentFAB" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydKYC"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "FLoydAVP"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BusinessUnitHead"
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    When I navigate to 'CaseDetails' task screen
    #Test-data: Validate "Waiting for UID from GLCMS" task is generated
    When I see "Waiting for UID from GLCMS" task is generated
    When I see 'Casedetails' task screen is auto-refreshed and "Waiting for T24 CIF ID from GLCMS" task status is updated as completed under task grid
    When I click on 'Navigate' option displaying under options button for "Waiting for T24 CIF ID from GLCMS" task
    When I see "Capture Fab references" task screen is displayed
    #Test-data:  Validate "Status" field is auto-populated as "Error" if Fenergo does not  receive "GLCMS UID"
    When I see 'Status' field under grid is auto-populated as 'Error'
    #Test-data: Validate 'Resend' button display as enabled for KYC Maker on "Capture Fab references" task when "Status" field is auto-populated as "Error"
    Then I validate 'Resend' button is displaying as enabled
    #Test-data: Validate after Resending the request, the 'Resend' button display as disabled
    Then I Validate after resending the request 'Resend' button display as disabled
    #Test-data:Validate "Status" field is auto-populated as "Error" again as Fenergo does not  receive "GLCMS UID"
    When I see 'Status' field under grid is auto-populated as 'Error' again
    When I validate Resend button gets enabled again
    #Test-data: Send the request 10 times by clicking on 'resend' button 
    #Test-data: Verify 'Resend' button is not available after clicking on Resend button for 10 attempts
    When I resend the request 10 times
    Then I validate that resend button is not available on 'capture Fab request' task screen after 10 attempts
    #Test-data: validate all the 10 records are displaying under the grid on 'capture Fab reference' task screen
    And I validate that all the 10 records are displaying under the grid
    
    
    
    
    
    
    
    
    
      
    
    