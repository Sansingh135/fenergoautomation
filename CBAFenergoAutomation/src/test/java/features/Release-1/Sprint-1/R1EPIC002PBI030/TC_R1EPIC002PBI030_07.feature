#Test Case: TC_R1EPIC002PBI030_07
#PBI: R1EPIC002PBI030
#User Story ID: US075, US083
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_07

  @Automation
  Scenario: Validate LOVs for below mentioned fields under "Business details" section on "Enrich KYC Profile" task for Onboarding Maker
    Given I login to Fenergo Application with "RM"
   When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    # Test data- Validate LOVs for "Anticipated Transactions Turnover (Annual in AED)" and "Active Presence in Sanctioned Countries/Territories" under "Business details" section
     And I verify "Anticipated Transactions Turnover (Annual in AED)" drop-down values
