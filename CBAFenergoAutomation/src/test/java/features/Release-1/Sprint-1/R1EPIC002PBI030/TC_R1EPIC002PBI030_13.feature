#Test Case: TC_R1EPIC002PBI030_13
#PBI: R1EPIC002PBI030
#User Story ID: US083
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_13

  Scenario: Validate for conditional field "Active Presence in Sanctioned Countries/Territories" under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
    Given I login to Fenergo Application with "RM"
    When I create a new request with FABEntityType as "Financial Institution (FI)" and LegalEntityRole as "Client/Counterparty"
     And I complete "CaptureNewRequest" with Key "C3" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    # Test data- Validate conditional field "Active Presence in Sanctioned Countries/Territories" is visible and its LOVs are as per DD under "Source Of Funds And Wealth Details" section
    Then I check that below data is visible
    |FieldLabel|
    |Active Presence in Sanctioned Countries/Territories|
    
    