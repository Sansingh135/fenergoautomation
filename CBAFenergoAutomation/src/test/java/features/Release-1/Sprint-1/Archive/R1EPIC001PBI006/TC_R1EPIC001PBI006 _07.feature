Feature: TC_R1EPIC001PBI006 _07

Scenario: Verify the Business Unit Head (N3) user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "BusinessUnitHead(N3)User" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any Business Unit Head (N3) user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the Business Unit Head (N3) user
	Then I validate the search grid data
