#Test Case: TC_R1EPIC002PBI015_06
#PBI: R1EPIC002PBI015
#User Story ID: US112
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate if the field and LOVs for "Client Type" is as per DD on "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	When I navigate to "Search for Duplicates" screen by clicking on "search" button
	#Test Data: validate (sequence,  LOV , Visibility/editability/mandatory) of  "Client Type" is as per DD
	Then I can see "Client Type" field (sequence,LOV,Visibility/editability/mandatory) is diplaying as mentioned in DD