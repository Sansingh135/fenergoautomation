#Test Case: TC_R1EPIC002PBI005_03
#PBI: R1EPIC002PBI005
#User Story ID: US089,US093,US041,US092
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI005_03-Enrich KYC profile task -Customer Details

  @Automation
  Scenario: 
    Verify the conditionally available field behaviour of new fields added in Customer Details section of Enrich KYC Profile Screen

    #=Entity Level
    #=Emirate
    #=Previous Name(s)
    #=Swift Address
    #=Create entity with Entity type = FI or NBFI and Country of Incorporation = UAE and set 'yes' for the field 'Does the entity have a previous name(s)?'
    Given I login to Fenergo Application with "RM:FI"
    #=Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new request with FABEntityType as "Financial Institution (FI)" and LegalEntityRole as "Client/Counterparty"
    ##=And I fill the data for "CaptureNewRequest" with key "C1"
    And I complete "CaptureNewRequest" with Key "FI" and below data
    | Product | Relationship |
    | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    #Entity level field will be Conditionality available when FAB Entity Type = FI or NBFI
    #Emirate field will be available and read only at this stage if country of incorporation is UAE
    #Previous Name(s) filed will be Visible and mandatoy if Does the entity have a previous name(s)? = Yes
    #Swift Address field will be visible when FAB Entity type = FI or NBFI
    When I select "Yes" for "Dropdown" field "Does the entity have a previous name(s)?"
    And I validate the following fields in "CaptureRequestDetails" Sub Flow
      | Label             | FieldType | Mandatory | Visible | DefaultsTo | ReadOnly | 
      | Previous Name(s)  | TextBox   | true      | true    | NA         | NA       | 
      | Entity Level      | Dropdown  | true      | true    | NA         | NA       | 
      | Emirate           | Dropdown  | true      | true    | Select...  | NA       | 
      | SWIFT Address     | TextBox   | true      | true    | NA         | NA       | 