#Test Case: TC_R1EPIC01PBI001_09
#PBI: R1EPIC01PBI001
#User Story ID: US013
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS

Feature: Products Section- Product Grid Remove Products

@Onboarding 
Scenario: Verify Remove option is not visible for Product Grid in action button for Onboarding Checker

	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with ClientEntityType as "NBFI"  and ClientEntityRole as "Client/Counterparty"
	When I add a product in "CaptureNewRequest" screen
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360
	
	#complete all tasks of RM and Onboarding Maker
	
	Then I login to Fenergo Application with "OnboardingChecker"
	
	Then I navigate to "CaptureRequestDetails" task in action button
	Then I click on action button for "ProductGrid"
	Then I see the "Remove" option is not present in action button
	
	Then I navigate to "ReviewRequest" task in action button
	Then I click on action button for "ProductGrid"
	Then I see the "Remove" option is not present in action button
	
	Then I navigate to "ReviewRequest" task in action button
	Then I click on action button for "ProductGrid"
	Then I see the "Remove" option is not present in action button
	
	Then I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I click on action button for "ProductGrid"
	Then I see the "Remove" option is not present
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
