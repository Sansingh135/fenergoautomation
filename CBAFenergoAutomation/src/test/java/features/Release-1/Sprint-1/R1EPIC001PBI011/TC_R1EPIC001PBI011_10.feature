#Test Case: TC_R1EPIC01PBI011_10
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI011_10 

@TC_R1EPIC01PBI011_10
Scenario: Validate "Accounts" subflow is displaying as hidden for following users on "Validate KYC and Regulatory data" task
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	And I store the "CaseId" from LE360
	When I complete "ReviewRequest" task
	Given I login to Fenergo Application with "OnboardingMaker"
	Then I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
	Then I can see "Accounts" subflow is hidden 
    Given I login to Fenergo Application with "OnboardingChecker"
	When I search for the "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden 
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "FLODAVP"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "BusinessUnitHead(N3)"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "FLODVP"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "FLODSVP"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "Business Head (N2)"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden
	When I login to Fenergo Application with "Compliance"
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task
	Then I can see "Accounts" subflow is hidden