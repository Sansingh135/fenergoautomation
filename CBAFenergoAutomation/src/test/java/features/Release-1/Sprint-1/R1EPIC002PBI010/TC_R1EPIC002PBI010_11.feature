#Test Case: TC_R1EPIC002PBI010_11
#PBI: R1EPIC002PBI010
#User Story ID: US098
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1EPIC002PBI010_11

Scenario: Validate if Onboarding Maker is able to cancel Anticipated Transaction "Corporate" entity type  &
					Validate if Onboarding Maker is able to edit Anticipated Transaction after getting referred back


	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create a new request with FABEntityType as "Financial Institution (FI)" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	 Then I store the "CaseId" from LE360
    Then I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task  
   When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
	When I navigate to "EnrichKYCProfileGrid" task 
	
	#Navigate to Anticipated Transactional Activity (Per Month) subflow using "+" button
	And I navigate to "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I enter data in the screen
	And I click Cancel button
	
	#On clicking Cancel button OnboardingMaker should be taken back to "Enrich KYC profile" screen
	And I assert that user is back to "Enrich KYC profile" screen
	
	And I add "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	
	When I complete "EnrichKYCProfileFAB" task
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task  
	
	And I refer back to "Enrich Client Information" stage
	
	
	
	When I navigate to "EnrichKYCProfileGrid" task 
	
	And I edit "Anticipated Transactional Activity (Per Month)" from "Enrich KYC profile" screen
	And I assert that the edited transaction is visible in the grid "Anticipated Transactional Activity (Per Month)" with the following columns:
		|ID | Value of Projected Transactions (AED) | Number of Transactions  | Type/Mode of Transactions | Transaction Type | Currencies Involved | Countries Involved|  
	
	When I complete "EnrichKYCProfileFAB" task 