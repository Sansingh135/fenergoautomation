#Test Case: TC_R1EPIC002PBI302_01
#PBI: R1EPIC002PBI302
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI302_01 

@Automation 
Scenario: 
	Clienttype-BBG:Validate the field behaviours in Customer Details section and GLCMS section of Capture Request Details and Review request screen 
	#Validate the field behaviours in KYC Conditions section of Validate KYC and Regulatory screen
	Given I login to Fenergo Application with "RM:BBG" 
	#Create entity with client type = Business Banking Group
	When I complete "NewRequest" screen with key "BBG" 
	#Validate the below fields ae hidden in customer details section
	And I check that below data is not visible 
		| FieldLabel                                                      | 
		| Is this entity publicly listed?                                 | 
		| Name of Stock Exchange                                          |  
		| Country of Stock Exchange                                       | 
		| Is this entity regulated?                                       | 
		| Regulated By                                                    | 
		| Regulatory ID                                                   | 
		| FAB Segment                                                     | 
		| Group name                                                      | 
		| SWIFT BIC                                                       | 
		|GLCMS|
	And I complete "CaptureNewRequest" with Key "BBG" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	And I store the "CaseId" from LE360 
	And I search for the "CaseId" 
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "aerbhms" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
	And I store the "CaseId" from LE360
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "AERBMS" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "01345" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "sa.yth" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "sa yth" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
	And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "sa.yth" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	And I search for the "CaseId" 
    And I navigate to "CaptureRequestDetailsGrid" task 
	When I select "255" for "TextBox" field "Legal Entity Name" 
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
#	Then I store the "CaseId" from LE360 
#	 Given I login to Fenergo Application with "KYCMaker: BBG"
#	 And I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	#Validate Country of Domicile is hidden from the screen
	And I check that below data is not visible 
		| FieldLabel         | 
		|Country Of Domicile |
