#Test Case: TC_R1S2EPIC006PBI002_01
#PBI: R1S2EPIC006PBI002
#User Story ID: 5,7
#Designed by: Anusha PS
#Last Edited by: Priyanka Arora
#Note: The same test data can be used for first 3 test cases of this PBI.
@To_be_automated
Feature: TC_R1S2EPIC006PBI002_01

  Scenario: 
    Validate metadata and LOVs for the fields under "Adverse Media" and "Sanctions" in Complete AML task - Assessment - Fircosoft Screening

    Given I login to Fenergo Application with "RM:FI"
    #Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    When I complete "NewRequest" screen with key "FI" 
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "KYCMaker: FIG"
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYCProfileFAB" task
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    #Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
    And I add "FircosoftScreening" for the entity
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    And I navigate to "Assessment" screen
    #To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
    And I navigate to "FircosoftScreening" screen
    #Validation for Adverse Media
    And I check that below data is visible
      | FieldLabel    |
      | Adverse Media |
    #Refer Screen Mock Up - New tab in PBI
    And I validate the following fields in "Adverse Media" Sub Flow
      | Fenergo Label Name   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Adverse Media Status | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I verify "Adverse Media" drop-down values
    And I check that below data is not visible
      | Adverse Media Category |
    And I validate 'Adverse MediCategory' is not visible
    And I select "Positive Match" for "Adverse Media Status" field
    And I check that below data is not visible
      | Adverse Media Category |
    And I validate the following fields in "Adverse Media" Sub Flow
      | Fenergo Label Name     | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | Adverse Media Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I verify "Adverse Media" drop-down values
    And I select "No Match" for "Adverse Media Status" field
    And I validate 'Adverse Media Category' is not visible
    #Validation for Sanctions
    And I validate "Sanctions" is the Third section under "Fircosoft Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the following fields in "Adverse Media" Sub Flow
      | Label Name       | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | Sanctions Status | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I verify "Sanctions Status" drop-down values
    And I check that below data is not visible
      | Sanctions Category |
    And I select "Positive Match" for "Sanctions Status" field
    And I validate 'Sanctions Category' is visible
    And I validate the conditional field in "Sanctions" section
      | Fenergo Label Name | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | Sanctions Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "Sanctions Category" field
    #Refer PBI for LOV list
    And I select "No Match" for "Sanctions Status" field
    And I validate 'Sanctions Category' is not visible
