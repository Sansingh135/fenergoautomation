#Test Case: TC_R1S2EPIC006PBI002_04
#PBI: R1S2EPIC006PBI002
#User Story ID: 4,11,12,13
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1S2EPIC006PBI002_04

  Scenario: Validate if "Onboarding Maker" is able to delete/edit firocsoft screening
     #=Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate" 
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    And I search for the "CaseId"
    And I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button

    When I navigate to "EnrichKYCProfileGrid" task
    And I complete "AddAddress" task
    Then I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I add "AssociatedParty" via express addition
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
    #And I add Fircosoft Screening for the entity
    #And I assert that "Assessment Status" of the added screening is "In Progress"
    #To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    #And I navigate to "Assessment" screen
    #And I assert that "Status" of the added screening is "In Progress"
    #And I navigate back to "CompleteAML" task
    #Validate delete scenario as below
    #And I add Fircosoft Screening for the entity #second screening
    #And I assert that "Assessment Status" of the added screening is "In Progress"
    #And I navigate to "Assessment" screen
    #And I assert that "Status" of the added screening is "In Progress" #for the second screening
    #And I navigate to "Fircosoft Screening" screen #for the second screening
    #And I save and complete the screening #for the second screening
    #And I'm taken back to "CompleteAML" task
    #And I assert that "Assessment Status" of the added screening is "Complete"
    #Validate edit scenario of Associated Party as below
    #And I add Fircosoft Screening for the associated party
    #And I assert that "Assessment Status" of the added screening is "In Progress"
    #And I navigate to "Assessment" screen
    #And I assert that "Status" of the added screening is "In Progress"
    #And I navigate to "Fircosoft Screening" screen
    #And I navigate back to "CompleteAML" task
    #And I save and complete the screening
    #And I'm taken back to "CompleteAML" task
    #And I assert that "Assessment Status" of the added screening is "Complete"
    #And I add Fircosoft Screening for the associated party #again
    #And I assert that "Assessment Status" of the added screening is "In Progress"
    #complete the screening
    #Do the edit scenario validation after referring back from later stages as well
#    
#    
