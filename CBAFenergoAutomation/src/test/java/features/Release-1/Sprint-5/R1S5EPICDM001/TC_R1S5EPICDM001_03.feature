#Test Case: TC_R1S5EPICDM001_03
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Data Migration Screen

 
  Scenario: Validate if the DM user have 2 screens to complete data migration activity so that he/she can capture values of a CIB customer required in screen-1 and link associations of a CIB customer by adding existing/ creating new legal entities in screen-2
  #Placeholder test case 
  #Validate screen 1 is available for entering all details except association and screening - This can be accessed using DM user
  #Validate screen 2 is available for entering association and screening - This will be given as a url as per discussion with PC
  