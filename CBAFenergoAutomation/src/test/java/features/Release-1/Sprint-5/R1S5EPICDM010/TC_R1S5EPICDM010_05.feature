#Test Case: TC_R1S5EPICDM010_05
#PBI: R1S5EPICDM010
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add a Anticipated Transactional Activity (Per Month) subflow (DM screen - 1)

  Scenario: Validate "Anticipated Transactional Activity (Per Month)" subflow is not visible for RM user on "DM screen-1 (Capture LE details)" for "FI" client Type
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    #Test-data: Validate "Anticipated Transactional Activity (Per Month)" subflow is not visible for Onboarding maker on "DM screen-1 (Capture LE details)" for "FI" client Type
    And I Validate "Anticipated Transactional Activity (Per Month)" subflow is not visible
    