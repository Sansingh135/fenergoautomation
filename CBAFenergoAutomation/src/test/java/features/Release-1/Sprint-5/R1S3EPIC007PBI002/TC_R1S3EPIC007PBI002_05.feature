#Test Case: TC_R1S3EPIC007PBI002_05
#PBI: R1S3EPIC007PBI002
#User Story ID: D1.10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Documents Search

  @Tobeautomated
  Scenario: Verify Onboarding maker is able to search a document from Fenergo that is uploaded in DMS system using  below search criteria on Document details screen of "Capture Request Details", "KYC Document Requirements", "Complete ID&V"
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnBoardingMaker"
    When I search for "Caseid"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I complete to "EnrichKYCProfileGrid" task as "OnboardingManager"
    When I navigate to "KYCDocumentrequirement" task.
    When I click on "AttachDocument" from options button displaying corresponding to document requirement
    When I navigate to "DocumentDetails" screen
    #Test-data: validate user is able to search for a document with "document case id"  and add the same
    When I select document source as 'existing' and enter 'documentcaseID' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Test-data: validate user is able to search for a document with "LegalEntityname"  and add the same
    When I select document source as 'existing' and enter 'LegalEntityname' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Test-data: validate user is able to search for a document with "documentID"  and add the same
    When I select document source as 'existing' and enter 'documentID' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Test-data: validate user is able to search for a document with "BIC Code (SWIFT)"  and add the same
    When I select document source as 'existing' and enter 'BICCode(SWIFT)' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Test-data: validate user is able to search for a document with "BIC Code (SWIFT)"  and add the same
    When I select document source as 'existing' and enter 'CIFID(T24 CIF ID)' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Test-data: validate user is able to search for a document with "UID ID (GLCM UID)"  and add the same
    When I select document source as 'existing' and enter 'UIDID(GLCM UID)' associated with existing document in system
    When I click on 'search' button
    Then I can see document is appearing in the search results
    When I select the document and click on 'Save' button
    Then I can see selected document is added successfully
    #Repeat the same validation for all the remaining 7 screens
    #New Request>Capture Request Details> Document Details
		#Enrich KYC Info> KYC Document Requirements > Document Details
		#AML>Complete ID&V>Edit Verification>Document Details
		#AML > Complete AML > Document Details
		#Capture Request Details > Product > Document Details
		#Enrich KYC Profile > Tax Identifier > Document Details
		#AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details
