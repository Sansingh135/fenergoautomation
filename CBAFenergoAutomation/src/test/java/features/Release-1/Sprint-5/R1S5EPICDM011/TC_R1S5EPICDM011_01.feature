#Test Case: TC_R1S5EPICDM011_01
#PBI: R1S5EPICDM011
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to receive a reconcilation file from Fenergo

  Scenario: Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "Corporate"exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
  ##Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "FI" exported through "DM recon File" option on "DM Screen-3(Generate Reconciliation File)" screen
  ##Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "NBFI" exported through "DM recon File" option on "DM Screen-3(Generate Reconciliation File)" screen
  ##Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "BBG" exported through "DM recon File" option on "DM Screen-3(Generate Reconciliation File)" screen
  ##Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "PCG Entity" exported through "DM recon File" option on "DM Screen-3(Generate Reconciliation File)" screen
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Verify user is able to add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
    
    #Test-data: Verify user is able to add doc for "Documents" sub-flow
    When I click on plus button displaying at the top of "Documents" screen
    When I navigate to "AddDocuments" task
    When I select 'upload' from "document source" field and upload a document and save the details
    And I validate Document is added successfully
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Verify user is able to Add Addresses
    When I click on plus button displaying at the top of "Addresses" sub-flow
    When I navigate to "AddAddress" task
    Then I enter all the required details and click on save
    And I see Address is added to Address grid successfully
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
 		
 		
 	Scenario: Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "FI" exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
  Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Verify user is able to add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
    
    #Test-data: Verify user is able to add doc for "Documents" sub-flow
    When I click on plus button displaying at the top of "Documents" screen
    When I navigate to "AddDocuments" task
    When I select 'upload' from "document source" field and upload a document and save the details
    And I validate Document is added successfully
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Verify user is able to Add Addresses
    When I click on plus button displaying at the top of "Addresses" sub-flow
    When I navigate to "AddAddress" task
    Then I enter all the required details and click on save
    And I see Address is added to Address grid successfully
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.  
  
  Scenario: Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "NBFI" exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
  Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "NBFI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Verify user is able to add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
    
    #Test-data: Verify user is able to add doc for "Documents" sub-flow
    When I click on plus button displaying at the top of "Documents" screen
    When I navigate to "AddDocuments" task
    When I select 'upload' from "document source" field and upload a document and save the details
    And I validate Document is added successfully
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Verify user is able to Add Addresses
    When I click on plus button displaying at the top of "Addresses" sub-flow
    When I navigate to "AddAddress" task
    Then I enter all the required details and click on save
    And I see Address is added to Address grid successfully
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
  
  
  Scenario: Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "BBG" exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
  Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "BBG" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Verify user is able to add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
    
    #Test-data: Verify user is able to add doc for "Documents" sub-flow
    When I click on plus button displaying at the top of "Documents" screen
    When I navigate to "AddDocuments" task
    When I select 'upload' from "document source" field and upload a document and save the details
    And I validate Document is added successfully
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Verify user is able to Add Addresses
    When I click on plus button displaying at the top of "Addresses" sub-flow
    When I navigate to "AddAddress" task
    Then I enter all the required details and click on save
    And I see Address is added to Address grid successfully
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
  
  Scenario: Verify generated data (DM LE having data in all sub-flows) in XML file for Client Type "PCG Entity" exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
  Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "PCG Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Verify user is able to add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    When I click on plus button displaying at the top of "AnticipatedTransactionalActivity(Per Month)" sub-flow
    Then I navigate to "AnticipatedTransactionalActivity(Per Month)" task screen
    Then I enter all required details and click on save button
    And I validate record is added successfully
    
    #Test-data: Verify user is able to add doc for "Documents" sub-flow
    When I click on plus button displaying at the top of "Documents" screen
    When I navigate to "AddDocuments" task
    When I select 'upload' from "document source" field and upload a document and save the details
    And I validate Document is added successfully
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Verify user is able to Add Addresses
    When I click on plus button displaying at the top of "Addresses" sub-flow
    When I navigate to "AddAddress" task
    Then I enter all the required details and click on save
    And I see Address is added to Address grid successfully
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
 		
 		
 		
 		
 		
 		
 		
    
 
 
 
 
 