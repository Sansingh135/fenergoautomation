#Test Case: TC_R1S5EPICDM009_01
#PBI: R1S5EPICDM008
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM Screen 2 - Fircosoft screening 

 
  Scenario: Validate metadata and LOVs for the fields Assessment - Fircosoft Screening screen 
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM Screen-2 #using url
  And I provide value for "T24 CIF ID" field 
  And I click on "SEARCH" button
  And I assert the migrated legal entity is fetched and shown in the result grid
 	And I click on button "Add Selected"
 	And I am redirected to "Capture Hierarchy Details" screen
 	And I assert the migrated entity is visible in the hierarchy
 	#Add Fircosoft Screening by clikcing the actions(...) button from tree view and click "Add Fircosoft Screening"
	And I add "FircosoftScreening" for the entity 
	#To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
	And I navigate to "Assessment" screen 
	#To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
	And I navigate to "FircosoftScreening" screen 
	
	#Validation for Adverse Media
	And I validate "PEP" is the Second section under "Fircosoft Screening Summary" 
	And I validate the following fields in "Adverse Media" Sub Flow 
		| Fenergo Label Name   | Field Type | Visible | Editable | Mandatory | Field Defaults To |
		| Adverse Media Status | Drop-down  | Yes     | Yes      | Yes       | Select...         |
	And I validate LOVs of  "Adverse Media" field
	    | No Match       |
      | False Match    |
      | Positive Match | 
	And I validate 'Adverse Media Category' is not visible 
	And I select "Positive Match" for "Adverse Media Status" field 
	And I check that below data is not visible 
		|Adverse Media Category|
	And I validate the following fields in "Adverse Media" Sub Flow 
		| Fenergo Label Name     | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
		| Adverse Media Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
	And I validate LOVs of "PEP Category" field
    #Refer Screening PBI (FENERGOFAB-535) for LOV list
  And I select "No Match" for "Adverse Media Status" field 
	And I validate 'Adverse Media Category' is not visible 
	
	#Validation for PEP
    And I validate "PEP" is the Second section under "Fircosoft Screening Summary" 
    And I validate the only following field in "PEP" section
      | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | PEP Status         | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "PEP Status" field
      | No Match       |
      | False Match    |
      | Positive Match |
    And I validate 'PEP Category' is not visible
    And I select "Positive Match" for "PEP Status" field
    And I validate 'PEP Category' is visible
    And I validate the conditional field in "PEP" section
      | Fenergo Label Name | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | PEP Category       | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "PEP Category" field
      #Refer Screening PBI (FENERGOFAB-535) for LOV list
    And I select "False Match" for "PEP Status" field
    And I validate 'PEP Category' is not visible
	
	#Validation for Sanctions
	And I validate "Sanctions" is the Third section under "Fircosoft Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the following fields in "Adverse Media" Sub Flow 
		| Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
		| Sanctions Status   | Drop-down  | Yes     | Yes      | Yes       | Select...         |
	And I verify "Sanctions Status" drop-down values 
	And I check that below data is not visible 
		|Sanctions Category| 
	And I select "Positive Match" for "Sanctions Status" field 
	And I validate 'Sanctions Category' is visible 
	And I validate the conditional field in "Sanctions" section 
		| Fenergo Label Name | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
		| Sanctions Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
	And I validate LOVs of "Sanctions Category" field 
	  #Refer Screening PBI (FENERGOFAB-535) for LOV list
	And I select "No Match" for "Sanctions Status" field 
	And I validate 'Sanctions Category' is not visible 
 
  #Validation for FAB Internal Watch List
    And I validate "FAB Internal Watch List" is the fourth section under "Fircosoft Screening Summary" #Refer Screen Mock Up - New tab in PBI
    And I validate the only following field in "FAB Internal Watch List" section
      | Fenergo Label Name             | Field Type | Visible | Editable | Mandatory | Field Defaults To |
      | FAB Internal Watch List Status | Drop-down  | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "FAB Internal Watch List Status" field
      | No Match       |
      | False Match    |
      | Positive Match |
    And I validate 'FAB Internal Watch List Category' is not visible
    And I select "Positive Match" for "FAB Internal Watch List Status" field
    And I validate 'FAB Internal Watch List Category' is visible
    And I validate the conditional field in "FAB Internal Watch List" section
      | Fenergo Label Name               | Field Type             | Visible | Editable | Mandatory | Field Defaults To |
      | FAB Internal Watch List Category | Multi Select Drop-down | Yes     | Yes      | Yes       | Select...         |
    And I validate LOVs of "FAB Internal Watch List Category" field
    #Refer Screening PBI (FENERGOFAB-535) for LOV list
    And I select "False Match" for "FAB Internal Watch List Status" field
    And I validate 'FAB Internal Watch List Category' is not visible
 