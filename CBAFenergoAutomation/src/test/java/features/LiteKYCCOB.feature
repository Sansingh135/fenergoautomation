Feature: Lite KYC Onboarding for FAB

  @Onboardingflow @Sanity @Automation
  Scenario: Login and Creating an Onboarding flow for an FAB application
    Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "LiteKYC-FI"
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    
    When I complete "EnrichKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I assert only the following document requirements are listed for LiteKYC WF
      | KYC Document Requirement                                                                     | Default Document Type                                                                        | Default Document Category | Mandatory |
      | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Constitutive              | True      |
      | Wolfsberg Questionnaire                                                                      | Wolfsberg Questionnaire                                                                      | MISC                      | False     |
      | Others                                                                                       | Others                                                                                       | MISC                      | False     |
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    
		When I navigate to "CompleteID&VGrid" task 
		When I complete "ID&V" task 
		When I complete "EditID&V" task 
		When I complete "AddressAddition" in "Edit Verification" screen 
		When I complete "Documents" in "Edit Verification" screen 
		When I complete "TaxIdentifier" in "Edit Verification" screen 
		When I complete "LE Details" in "Edit Verification" screen 
		When I click on "SaveandCompleteforEditVerification" button 
		When I complete "CompleteID&V" task 
	
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "LiteKYC"
    
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
