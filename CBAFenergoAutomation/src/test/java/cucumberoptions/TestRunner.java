package cucumberoptions;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		features= {"src/test/java/features/Release-1/Sprint-1/R1EPIC001PBI013/TC_R1EPIC001PBI013_02.feature"},
		glue={"classpath:parallel"},
		strict=true,
		monochrome=true, dryRun = false,
		plugin= {"pretty","html:target/site/cucumber-pretty","json:target/cucumber/cucumber.json"},
        tags={"@Automation"}
		)
//extends AbstractTestNGCucumberTests 
public class TestRunner extends AbstractTestNGCucumberTests  {

     
    
	
	
}
